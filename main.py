import random

from os import environ


DEBUG = environ.get('DEBUG') == 'True'


def log(*data):
    if not DEBUG:
        return

    print(*data)


def create_topic():
    topic = random.sample(range(10), 4)
    while topic[0] == 0:
        topic = random.sample(range(10), 4)

    log(topic)
    return topic


def user_input():
    while True:
        number = input('請輸入你要猜的數字 (一個不重複四位數字)：')

        if not number.isdigit():
            print('輸入的並非數字，請重新輸入')
            continue

        if len(int(number)) != 4:
            print('輸入長度不符合規定，請重新輸入')
            continue

        flag = False
        for i in number:
            if number.count(i) > 1:
                flag = True
                break

        if flag:
            print('您輸入的數字有重複，請重新輸入')
            continue

        return number


def get_result(topic, number):
    a = 0
    b = 0
    for i, j in zip(topic, number):
        if i == int(j):
            a += 1
            continue

        if int(j) in topic:
            b += 1

    return a, b


def main():
    topic = create_topic()
    count = 0
    while True:
        count += 1
        print('\n第 {} 次輸入'.format(count))
        number = user_input()
        a, b = get_result(topic, number)
        print('您輸入 {} 結果為 {}A{}B'.format(number, a, b))
        if a == 4:
            print('恭喜你猜中了！您一共猜了 {} 次'.format(count))
            break



if __name__ == '__main__':
    main()
